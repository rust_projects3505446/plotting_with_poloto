use tagu::format_move;
use poloto::build;
use std::fs::write;
use svg2pdf::{convert_str,Options};
fn main() {
    // hourly trend over one day.
    let trend = vec![
        2.327473685,
        38.339006423950195,
        0.258953
    ];

    let plots = poloto::plots!(
        build::plot("").histogram((0..).zip(trend)),
        build::markers([6], [])
    );

    let data = poloto::frame_build().data(plots);

    let ticks =
        poloto::ticks::from_iter((0..).step_by(1)).with_tick_fmt(|&v| format_move!("{} hr", v));
    let svg_data = data.map_xticks(|_| ticks)
        .build_and_label(("runtime comparision", "runtime in sec", "programming languages"))
        .append_to(poloto::header().light_theme())
        .render_string().unwrap();
    let pdf = convert_str(
        &svg_data,
        Options::default()
        ).unwrap();
    write("plot.pdf", pdf).unwrap(); 
}
