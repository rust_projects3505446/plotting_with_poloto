use std::fs::write;
use svg2pdf::{
    Options, 
    usvg,
    usvg::{fontdb, PostProcessingSteps, TreeParsing, TreePostProc},
};

fn main() {
    let data = [
        (2.327473685, "Rust-bio"),
        (38.339006423950195, "Python"),
        (0.258953, "Julia"),
    ];

    let svg = poloto::build::bar::gen_simple("", data, [0.00])
        .label(("Comparison of Programming Language Runtime", "Runtime in Seconds", "Languages"))
        .append_to(poloto::header().light_theme())
        .render_string().unwrap();

    let mut tree = usvg::Tree::from_str(&svg, &usvg::Options::default()).unwrap();

    let mut db = fontdb::Database::new();
    db.load_system_fonts();
    tree.postprocess(PostProcessingSteps::default(), &db);
    
    let pdf = svg2pdf::convert_tree(&tree, Options::default());

    write("bar.svg", svg).unwrap();
    write("bar.pdf", pdf).unwrap();
}
